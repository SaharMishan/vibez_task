import React, { useState } from "react";

// Chakra imports
import { Box, Flex, useColorModeValue, Button, Text } from "@chakra-ui/react";

// Custom components
import FormCard from "../../project/components/FormCard";
import FormikForm from "../../project/components/FormikForm";
import { AddButton } from "components/buttons/AddButton";

// Custom components
import { AddIcon } from "@chakra-ui/icons";

export default function NoProjectView({ projects }) {
  const [showForm, setShowForm] = useState(0);
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");

  // Chakra Color Mode
  return (
    <>
      {showForm == 1 ? (
        // <FormikForm activeMode={1} setShowForm={setShowForm} />
        <Flex
          w="100%"
          textAlign={"center"}
          justifyContent={"center"}
          alignItems={"center"}
          flexDirection="column"
        >
          <FormCard activeMode={1} setShowForm={setShowForm} />
        </Flex>
      ) : (
        <Flex
          w="100%"
          // h="50vh"
          direction="column"
          mb="30px"
          ms="10px"
          justifyContent={"center"}
          alignItems={"center"}
        >
          {projects?.length == 0 && (
            <Box textAlign={"center"} mb="20px">
              <Text fontSize="2xl" color={textColorPrimary} fontWeight="bold">
                Welcome
              </Text>
              <Text fontSize="md" color={textColorPrimary}>
                No projects yet, create your first project
              </Text>
            </Box>
          )}
          <AddButton text={"Create Project"} action={() => setShowForm(1)} />
        </Flex>
      )}
    </>
  );
}
